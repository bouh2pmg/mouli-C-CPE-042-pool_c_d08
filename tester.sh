#!/bin/bash

if [ $# -lt 1 ]
then
    echo -e "[ERREUR] $0 : pas assez d'arguments"
    echo -e "USAGE : $0 exercice [-c]"
    exit 2
fi

exercise=ex_$1
binary=$exercise.bin
output="output.test"

if [ ! -e $exercise ]
then
    echo "KO: exercise [$binary] not found"
    exit 2
fi

if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
then
    cd tests/$exercise && cc *.c -o $binary && cd - &> /dev/null
    ./tests/$exercise/$binary &> "./tests/${exercise}_REF"
else
    if [[ $exercise == "ex_01" ]] || [[ $exercise == "ex_03" ]]
    then
	sudo -u student cc ./tests/$exercise/moulinette.c -I./$exercise/ -o $binary	
	sudo -u student ./$binary &> output.test
	res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))
    elif [[ $exercise == "ex_02" ]] || [[ $exercise == "ex_04" ]]
    then
	sudo -u student cc ./$exercise/*.c ./tests/$exercise/moulinette.c -I./$exercise/ -o $binary	
	sudo -u student ./$binary &> output.test
	res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))
    elif [[ $exercise == "ex_09" ]]
    then
	# my_8_queen is a software
	sudo -u student cc ./$exercise/*.c -o $binary
	sudo -u student ./$binary &> output.test
	res=$(diff -a <(sort "./tests/${exercise}_REF" | cat -e) <(sort output.test | cat -e))
    else
	sudo -u student cc ./$exercise/*.c ./tests/$exercise/moulinette.c -o $binary
	sudo -u student ./$binary &> output.test
	res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))
    fi

    if [ $? -ne 0 ]; then
	echo "KO: check your traces below this line..."
	echo "$res"
    else
	echo "OK"
    fi
fi
