#include <unistd.h>

static int	abs_(int a, int b)
{
  return ((a >= b) ? (a - b) : (b - a));
}

static void	print_(int *tab)
{
  int		i;
  char		c;
  
  i = 0;
  while (i < 8)
    {
      c = tab[i] + 'a' - 1;
      write(1, &c, 1);
      c = '9' - i - 1;
      write(1, &c, 1);
      if (i != 7)
	write(1, ",", 1);
      ++i;

    }
  write(1, "\n", 1);
}

static void	place_queens_(int *tab, int n)
{
  int	i;
  int	j;
  
  i = 1;
  if (n == 8)
    print_(tab);
  while (i <= 8)
    {      
      j = 0;
      while (j < n && tab[j] != i && abs_(tab[j], i) != n - j)
	++j;
      if (j == n)
	{
	  tab[n] = i;
	  place_queens_(tab, n + 1);
	}
      ++i;
    }
}

int	my_8_queens()
{
  int	tab[8];

  place_queens_(tab, 0);
  return 0;
}

int	main()
{
  my_8_queens();
  return 0;
}
