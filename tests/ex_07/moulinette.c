// No Header
// 10/10/2017 - 11:43:06

#include <stdio.h>

int	fib_it(int);

int	main()
{
  int	res1;
  int	res2;
  int	res3;
  int	res4;

  
  res1 = fib_it(10);
  res2 = fib_it(0);
  res3 = fib_it(-4);
  res4 = fib_it(42);

  if (res1 != 10)
    printf("%d : Hey what you say !?\n", res1);
  printf("%d : NULL\n", res2);
  printf("%d : BibadiBabida Bouuuu !!!!\n", res3);
  printf("%d\n", res4);
}
